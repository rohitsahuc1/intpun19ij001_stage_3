import java.io.*;
import java.math.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;


public class Solution {

    /*
     * Complete the runningMedian function below.
     */
    static double[] runningMedian(int[] a) {
        /*
         * Write your code here.
         */
        //  double arr[] = new double[a.length];
        //  int temp = 0;
        //  arr[0] = a[0];
        //  for(int i = 1; i < a.length; i++) {
        //      temp = a[i];
        //      for(int j = 0; j < i; j++) {
        //          if(a[i] < a[j]) {
        //             temp = a[j];
        //             a[j] = a[i];
        //          }
        //      }
        //      a[i] = temp;
        //      if(i%2 != 0) {
        //         arr[i] = (a[i/2] + a[((i/2) + 1)]) / 2.0;
        //      }
        //      else {
        //          arr[i] = a[i/2];
        //      }
        //  }
        //  return arr;
        PriorityQueue<Integer> maxHeap = new PriorityQueue<>(Collections.reverseOrder());        PriorityQueue<Integer> minHeap = new PriorityQueue<>();                                  double arr[] = new double[a.length];
         for (int i = 0; i < a.length; i++) {
            if (maxHeap.isEmpty()) {
                maxHeap.add(a[i]);
            } else if (maxHeap.size() == minHeap.size()) {
                if (a[i] < minHeap.peek()) {
                    maxHeap.add(a[i]);
                } else {
                    minHeap.add(a[i]);
                    maxHeap.add(minHeap.remove());
                }
            } else if (maxHeap.size() > minHeap.size()) {
                if (a[i] > maxHeap.peek()) {
                    minHeap.add(a[i]);
                } else {
                    maxHeap.add(a[i]);
                    minHeap.add(maxHeap.remove());
                }
            }
            if (maxHeap.size() == minHeap.size()) {
                arr[i] =  (maxHeap.peek() + minHeap.peek()) / 2.0;
            } else {
                arr[i] = maxHeap.peek();
            }
        }
    return arr;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int aCount = Integer.parseInt(scanner.nextLine().trim());

        int[] a = new int[aCount];

        for (int aItr = 0; aItr < aCount; aItr++) {
            int aItem = Integer.parseInt(scanner.nextLine().trim());
            a[aItr] = aItem;
        }

        double[] result = runningMedian(a);

        for (int resultItr = 0; resultItr < result.length; resultItr++) {
            bufferedWriter.write(String.valueOf(result[resultItr]));

            if (resultItr != result.length - 1) {
                bufferedWriter.write("\n");
            }
        }

        bufferedWriter.newLine();

        bufferedWriter.close();
    }
}
